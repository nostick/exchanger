'use strict'

//Libraries
import express from 'express';
import bodyParser from 'body-parser';

//Load Models
import models from './api/models/models';

//Basics
let app = express();

//Config App
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//Load routes
import routes from './api/routes/routes';
routes(app);

module.exports = app;