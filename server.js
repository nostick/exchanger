// Utilizar funcionalidades del Ecmascript 6
'use strict';

// Cargamos el módulo de mongoose para poder conectarnos a MongoDB
import mongoose from 'mongoose';

// *Cargamos el fichero app.js con la configuración de Express
import app from './app';

// Environment variables
process.env.NODE_ENV = 'development';

// Server config vars
import config from './api/config/config';

// Creamos la variable PORT para indicar el puerto en el que va a funcionar el servidor
let port = global.env.node_port;

// Le indicamos a Mongoose que haremos la conexión con Promesas
mongoose.Promise = global.Promise;

// Load enviroment variables

// Usamos el método connect para conectarnos a nuestra base de datos
mongoose.connect('mongodb://mongo:27017/exchange', { useNewUrlParser: true })
    .then(() => {
        // Cuando se realiza la conexión, lanzamos este mensaje por consola
        console.log("La conexión a la base de datos exchange se ha realizado correctamente")

        // CREAR EL SERVIDOR WEB CON NODEJS
        app.listen(port, () => {
            console.log("servidor corriendo en http://localhost:"+port); 
        });
    })
    // Si no se conecta correctamente escupimos el error
    .catch(err => console.log(err));