'use strict';

import rs from '../services/responseService';

exports.testBasic = (req, res, next) => {
    rs.prepareResponse(res,{
        myName:"Cesar",
        myLast:"Sulbaran"
    });
    next()
}