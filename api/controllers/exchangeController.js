'use strict';

import mongoose from 'mongoose';
import axios from 'axios';

let Currency = mongoose.model('Currency');

exports.populateRates = (req, res) => {
    //https://api.currencyconverterapi.com/api/v6/convert?q=USD_PHP,PHP_USD&compact=ultra&apiKey=[YOUR_API_KEY]
    let options = {
        method: 'get',
        url: global.env.url_currencies,
    }

    axios(options)
        .then(response => {
            let result = response.data.results
            let data = [];
            for (const key in result) {
                let currentValue = result[key];

                if (result.hasOwnProperty(key)) {
                    data.push(currentValue)
                }
            }
            if (data.length > 0) {
                Currency.insertMany(data, (error, docs) => {
                    if (error) {
                        res.json(error.errmsg)
                    }
                    res.send("Successfully populated")
                })
            }
        })
        .catch(error => {
            res.send(error)
        })
}