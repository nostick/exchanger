'use strict';

import mongoose from 'mongoose'
const Schema = mongoose.Schema

const CurrencySchema = new Schema({
    id: {
        type: String,
        required: 'Please enter currency shorten name',
        unique: true
    },
    currencyName: {
        type: String,
        required: 'Please enter currency name'
    },
    currencySymbol: {
        type: String,
        required: 'Please enter currency Symbol', 
        default: 'No Symbol'
    },
    created_date: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Currency', CurrencySchema);