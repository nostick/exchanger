//Imports
import _ from 'lodash'; 

// module variables
const config = require('./config.json');
const defaultConfig = config.global;
const environment = process.env.NODE_ENV || 'development';
const environmentConfig = config[environment];
const finalConfig = _.merge(defaultConfig, environmentConfig);

global.env = finalConfig;