'use strict';

let response = {
    entry : null,
    status: null,
    data: null
}

module.exports = (app) => {
    app.use( (req, res, next) => {
        let entry = {query: req.query};
        if(req.method === 'POST'){
            entry.body = req.body
        }
        
        if(res.response){
            res.send({
                entry:entry,
                status:"My Status",
                data: res.response
            })
        } 
        res.status(404).send({url: req.originalUrl + ' not found'})
    })
}