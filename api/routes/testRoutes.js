'use strict';

import TestController from '../controllers/testController';

module.exports = (app) => {
    app.route('/test')
        .get(TestController.testBasic);
}