'use strict';

//Importing routes
import todoRoutes from './todoListRoutes';
import testRoutes from './testRoutes';
import exchangeRoutes from './exchangeRoutes';

//Importing middlewares
import rsMiddleware from '../middlewares/responseMiddleware'

module.exports = (app) => {
    //Add routes to app
    todoRoutes(app); 
    testRoutes(app);
    exchangeRoutes(app);

    //Load response middleware
    rsMiddleware(app);
}