'use strict';

import ExchangeController from '../controllers/exchangeController'

module.exports = (app) => {
    app.route('/populate-currencies')
        .get(ExchangeController.populateRates);
}